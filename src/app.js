const express = require('express'),
    app = express(),
    cors = require('cors'),
    morgan = require('morgan'),
    path = require('path'),
    bodyParser = require('body-parser'),
    mqtt = require('mqtt'),
    mqttClient = mqtt.connect('mqtt://0.0.0.0:1883'),
    mqttTopic = 'ecg',
    server = require('http').createServer(app),
    io = require('socket.io').listen(server);

const getData = require('./data')

var timeOffset;
var lastPublishedIndex = -1;
var streamInterval;
var msFrequency = 20;


/* 
Subscribe (listen) to MQTT topic and start publishing
simulated data after successful MQTT connection 
*/
mqttClient.on('connect', () => {
    console.log('MQ Telemetry Transport Connected')
    mqttClient.subscribe(mqttTopic); //subscribe
    startStreamSimulation(); //publish
})


mqttClient.on('offline', () => {
    console.log('MQ Telemetry Transport Disconnected')
    mqttClient.unsubscribe(mqttTopic);
    clearInterval(streamInterval);
})

/* 
Message event fires, when new messages
arrive on the subscribed topic
*/
mqttClient.on('message', function (topic, message) {
    // console.log(message.toString()); 
    let parsedMessage = JSON.parse(message);
    io.emit('ecgData', parsedMessage);
})

/* 
Function that publishes simulated data to the MQTT broker every ≈20ms
*/
function startStreamSimulation() {
    getData().then(function(ecgReadings) {
        const timestampNow = Date.now()
        timeOffset = Date.now() - ecgReadings[0].timestamp
        const data = ecgReadings.map(value => ({timestamp: value.timestamp + timeOffset, ecg: value.ecg}))
    
        streamInterval = setInterval(function () {
            if (lastPublishedIndex >= (data.length - 1)) { 
                console.log("No more records")
                clearInterval(streamInterval);
            }

            const groupUpToTimestamp = Date.now() + msFrequency
            const group = []

            for (let i = lastPublishedIndex + 1; i < data.length; i++) {
                if (data[i].timestamp < groupUpToTimestamp) {
                    group.push(data[i])
                    lastPublishedIndex = i
                } else {
                   break 
                }
            }
    
            /* Publish data for current group to the corresponding MQTT topic as a JSON string  */
            if (group.length) {
                mqttClient.publish(mqttTopic, JSON.stringify(group));
            }
        }, msFrequency);
    })
}

io.on('connection', (client) => {
    console.log("Connection Established.")
})

app.use(bodyParser.json()); // to support JSON-encoded bodies
app.use(bodyParser.urlencoded({ // to support URL-encoded bodies
    extended: true
}));

//setting middleware
app.use(express.static(__dirname + '/public')); //Serves resources from public folder
/*
Definition of route handler / to fetch home page(index.html)
*/
app.get('/', function(req, res){
  res.sendFile(__dirname + '/index.html'); //renders html file
});

server.listen(3000, function () {
    console.log('App listening on port 3000!');
});
