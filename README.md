# Real time data visualisation test

At Quro, one very critical aspect of the work is to ensure that Medical Practitioners have patient data in real time, and accurately. We need you to get to work, right away. Develop a reactjs app to display live client ecg data for a medical practitioner.

##This app uses MQTT and Socket.io for real-time update
It is running on Nodejs server.
For ploting the Graph I used Javascript framework called "Plot.ly" https://plot.ly/

##The resources of the App are inside the "src/public"

## To run it open on command line folder named "src" inside the root directory then execute: 
   *command nodemon app.js*
   Then go to your browser and open "localhost:3000" to view the visualization

## To take screenshot, zoom graph and other functions hover over graph the toolbar will show on top left of graph


## Please report bugs or errors to: fezekileplaatyi@gmail.com

#Cheers!